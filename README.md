# AppServer

TCP-Server для запуска\остановки Linux-приложений.
Для взаимодействия с сервером использовать команду

echo "command <app> opt:--argument" | nc localhost 7777

command - run или close
app - имя приложения
argument - Опциональный аргумент приложения.